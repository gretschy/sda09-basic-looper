//
//  Looper2.hpp
//  JuceBasicAudio
//
//  Created by Tom on 17/11/2016.
//
//

#ifndef Looper2_hpp
#define Looper2_hpp

#include <stdio.h>
#include "Looper.h"
#include "../JuceLibraryCode/JuceHeader.h"

class Looper2 : public Looper
{
public:
    
    /**constructor*/
    Looper2();
    
    /**destructor*/
    ~Looper2();
    
    
private:
    Atomic<int> playState;
    Atomic<int> recordState;
    static const int bufferSize = 88200; //constant
    unsigned int bufferPosition;
    float audioSampleBuffer[bufferSize];
    
};

#endif /* Looper2_hpp */
