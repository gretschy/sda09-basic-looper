//
//  Looper2.cpp
//  JuceBasicAudio
//
//  Created by Tom on 17/11/2016.
//
//

#include "Looper2.hpp"

Looper2::Looper2()
{
    //initialise - not playing / recording
    playState = false;
    recordState = false;
    //position to the start of audioSampleBuffer
    bufferPosition = 0;
    
    //audioSampleBuffer contents to zero
    for (int count = 0; count < bufferSize; count++)
        audioSampleBuffer[count] = 0.f;
}

Looper2::~Looper2()
{
    
}

