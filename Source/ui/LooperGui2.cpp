//
//  LooperGui2.cpp
//  JuceBasicAudio
//
//  Created by Tom on 17/11/2016.
//
//

#include "LooperGui2.hpp"

LooperGui2::LooperGui2(Looper2& looper2_) : looper2(looper2_)
{
    playButton2.setButtonText ("Play");
    playButton2.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton2.setColour(TextButton::buttonColourId, Colours::grey);
    playButton2.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton2);
    playButton2.addListener (this);
    
    recordButton2.setButtonText ("Record");
    recordButton2.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton2.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton2.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton2);
    recordButton2.addListener (this);
}

void LooperGui2::resized()
{
    playButton2.setBounds (0, 500, getWidth()/2, getHeight()/2);
    recordButton2.setBounds (playButton2.getBounds().translated(getWidth()/2, 0));
}

void LooperGui2::buttonClicked (Button* button)
{
    if (button == &playButton2)
    {
        looper2.setPlayState (!looper2.getPlayState());
        playButton2.setToggleState (looper2.getPlayState(), dontSendNotification);
        if (looper2.getPlayState())
            playButton2.setButtonText ("Stop");
        else
            playButton2.setButtonText ("Play");
    }
    else if (button == &recordButton2)
    {
        looper2.setRecordState (!looper2.getRecordState());
        recordButton2.setToggleState (looper2.getRecordState(), dontSendNotification);
        
    }
}