//
//  LooperGui2.hpp
//  JuceBasicAudio
//
//  Created by Tom on 17/11/2016.
//
//

#ifndef LooperGui2_hpp
#define LooperGui2_hpp

#include <stdio.h>
#include "Looper2.hpp"
#include "LooperGui.h"
#include "../JuceLibraryCode/JuceHeader.h"

class LooperGui2 : public LooperGui
{
public:
    /**constructor*/
    LooperGui2(Looper2& looper2_);
    
    void resized();
    void buttonClicked(Button* button);
    
private:
    Looper2& looper2;
    TextButton playButton2;
    TextButton recordButton2;
    
};


#endif /* LooperGui2_hpp */
